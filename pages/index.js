import Link from "next/link";
import Head from "next/head";
import styled from "@emotion/styled";
import { colors, fonts } from "../components/tokens.js";
import StyledGlobal from "../components/globalStyles";
import { Layout } from "../components/layout.js";

import { PostBlurb } from "../components/postBlurb.js";

export const Home = () => (
  <Layout title="home">
    <StyledAboutBlurb>
      <h2> Hello. This is my blog </h2>
      <p>
        {" "}
        I'm a human bean who like making things on the internet and IRL.
        <br></br>
        <br></br>
        One day I suddently felt a strong urge to type long essays into an
        endless and all consuming void. And so, I started a blog.{" "}
      </p>
    </StyledAboutBlurb>
    <StyledPostList>
      <PostBlurb
        postLink="./posts/001-hello-world"
        blurbTitle="Hello World"
        blurbDate="March 19, 2020"
        blurbText="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
        consequat pellentesque ullamcorper. Nullam gravida est et quam porta,
        sit amet dignissim quam aliquet. Pellentesque sit amet libero a leo
        rhoncus posuere et laoreet augue."
      />
    </StyledPostList>
  </Layout>
);

export default Home;

const StyledAboutBlurb = styled.div`
  margin-bottom: 2rem;
`;

const StyledPostList = styled.div`
  margin-bottom: 4rem;
`;
