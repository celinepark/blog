import Link from "next/link";
import Head from "next/head";
import styled from "@emotion/styled";
import { colors, fonts } from "./tokens.js";

const StyledGlobal = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,700,700i&display=swap");
  @import url("https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700,800&display=swap");
  /* @import url("https://fonts.googleapis.com/css?family=Raleway:500,500i,700,700i&display=swap"); */

  font-family: "Crimson Text", serif;
  color: ${colors.primaryText};
  margin: auto;
  padding: 0 2rem 6rem 2rem;
  max-width: 800px;

  h1 {
    /* only my name is h1 */
  }

  h2 {
    /* post titles, home page main headers */
    font-family: ${fonts.header};
    font-weight: 600;
    font-size: 1.666rem;
    text-transform: uppercase;
  }

  h3 {
    /* Post List titles, Post Subheader  */

    font-family: ${fonts.header};
    font-weight: 600;
    font-size: 1.2rem;
    text-transform: uppercase;
    color: ${colors.secondaryText};
    margin-top: 3rem;
    margin-bottom: 1rem;
  }

  p {
    font-family: ${fonts.body};
    font-weight: 300;
    font-size: 1.4rem;
    line-height: 2rem;
    margin-top: 0;
    margin-bottom: 1.4rem;
  }

  a {
    color: ${colors.linkText};
    text-decoration: none;
  }

  a:hover {
    color: ${colors.linkText};
    text-decoration: underline;
  }

  a:active {
    color: ${colors.linkTextActive};
    text-decoration: underline;
  }
`;

export default StyledGlobal;
