import Link from "next/link";
import Head from "next/head";
import styled from "@emotion/styled";
import { colors, fonts } from "../components/tokens.js";
import StyledGlobal from "../components/globalStyles";
import { Layout } from "../components/layout.js";

export const PostBlurb = props => (
  <Link href={props.postLink}>
    <StyledPostBlurb>
      <p className="blurbDate"> {props.blurbDate}</p>

      <h3 className="blurbTitle"> {props.blurbTitle} </h3>
      <p className="blurbText"> {props.blurbText}</p>
    </StyledPostBlurb>
  </Link>
);

const StyledPostBlurb = styled.div`
  border: 1px solid ${colors.bordercolor};
  border-radius: 8px;
  padding-left: 2rem;
  padding-right: 2rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  cursor: pointer;

  :hover {
    box-shadow: 5px 5px ${colors.linkText};
    .blurbTitle {
      color: ${colors.linkText};
    }
  }

  .blurbTitle {
    margin-bottom: 1rem;
    margin-top: 0;
  }

  .blurbDate {
    color: ${colors.secondaryText};
    font-size: 0.75rem;
    text-transform: uppercase;
    margin-bottom: 0;
    margin-top: 0.8rem;
  }

  .blurbText {
    color: ${colors.secondaryText};
  }
`;
