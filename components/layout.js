import Link from "next/link";
import Head from "next/head";
import styled from "@emotion/styled";
import { colors, fonts } from "./tokens.js";
import StyledGlobal from "./globalStyles";

export const Layout = ({ children, title = "This is the default title" }) => (
  <StyledGlobal>
    <StyledHeader>
      <div className="nameAndContact">
        <div>
          <h1 className="name">CELINE PARK</h1>
        </div>

        <div>
          <p className="contact">
            hmu:
            <a href="mailto:thisiscelinepark@gmail.com">
              {" "}
              <span id="headerEmail">thisiscelinepark@gmail.com</span>
            </a>
          </p>
        </div>
      </div>

      <div className="nav">
        <Link href="/">
          <a className="navItem"> \\ INDEX </a>
        </Link>
        <a href="https://celinepark.design/" className="navItem">
          {" "}
          \\ PORTFOLIO
        </a>
      </div>
    </StyledHeader>

    {children}

    {/* <StyledFooter>
      <p> goodbye </p>
    </StyledFooter> */}
  </StyledGlobal>
);

const StyledHeader = styled.div`
  color: ${colors.primaryText};
  font-family: ${fonts.header};

  display: grid;
  grid-template-columns: auto auto;
  align-items: center;
  margin-top: 48px;
  margin-bottom: 80px;

  .nameAndContact {
    padding: 0;
    margin-block-end: 0px;
  }

  .name {
    margin: 0;
    padding: 0;
    line-height: 2.5rem;
    letter-spacing: 4px;
    font-weight: 600;
    font-size: 2.45rem;
  }

  .contact {
    color: ${colors.secondaryText};
  /* font-family: ${fonts.header}; */
    margin: 0;
    margin-top: .20rem;
    padding: 0;
    line-height: 1rem;
  }

  .nav {
    margin: 0px;
    display: grid;
    grid-template-columns: auto auto;
    grid-column-gap: 24px;
    align-items: center;
    justify-self: end;
    justify-items: end;
    align-self: start;
  }

  .navItem {
    color: ${colors.secondaryText};
    font-family: ${fonts.header};
    font-size: 1.2rem;
  }

  @media (max-width: 700px) {
      grid-template-columns: 100%;
      grid-template-rows: auto auto;
      justify-items: left;

      .nav {
        margin-top: 30px;
        justify-self: start;
        justify-items: start;
      }
    }

  }
`;

const StyledFooter = styled.div`
  color: ${colors.primaryText};
`;
