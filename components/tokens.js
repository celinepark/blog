export const colors = {
  primaryText: "#2A2A2A",
  secondaryText: "#4f4f4f",
  linkText: "#C42C2C",
  linkTextActive: "#801414",
  bordercolor: "#c2c2c2"
};

export const fonts = {
  header: "'Work Sans', sans-serif",
  body: "'Crimson Text', serif"
};
